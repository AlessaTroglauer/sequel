using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTrigger : MonoBehaviour
{

    public GameObject textObject;

    void Start()
    {
        textObject.SetActive(false);

    }

    void OnTriggerEnter2D(Collider2D Player)
    {
        textObject.SetActive(true);
        StartCoroutine("CountDown");
    }

    IEnumerator CountDown()
    {
        yield return new WaitForSeconds(5);
        Destroy(textObject);
        Destroy(gameObject);
    }

}
