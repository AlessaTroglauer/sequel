using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public int destScene;

    void OnTriggerEnter2D(Collider2D other)
    {
        SceneManager.LoadScene(destScene);
    }

}