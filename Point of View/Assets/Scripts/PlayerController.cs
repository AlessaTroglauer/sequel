﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //Movement variables 
    [SerializeField]
    private float moveSpeed = default;
    private float moveInput = default;
    private bool facingRight = true; 

    //Jump and fall variables 
    [SerializeField]
    private float jumpForce = default;
    private Rigidbody2D rb2d = default;
    private bool canDoubleJump = default;
    [SerializeField]
    private float fallMultiplier = default;

    //Groundcheck Variables
    [SerializeField]
    private LayerMask groundLayerMask = default;
    private BoxCollider2D boxCollider2d = default;

    //Variables for Health System
    public static int health = 3;
    public int numberofHearts;
    [SerializeField] private Image[] hearts;
    [SerializeField] private Sprite fullHeart;
    [SerializeField] private Sprite emptyHeart;
    private bool Flipped = false;

    private void Start()
    {
        //Setting references 
        rb2d = GetComponent<Rigidbody2D>();
        boxCollider2d = GetComponent<BoxCollider2D>();
        health = 3;
    }
    private void Update()
    {
        //Set moveInput to Horizontal Axis Input
        moveInput= Input.GetAxis("Horizontal");
        
        //Flip player
        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if (facingRight == true && moveInput < 0)
        {
            Flip();
        }

        //Enable double jump when grounded
        if (IsGrounded())
        {
            canDoubleJump = true;
        }

        //Check if jump button is pressed
        if (Input.GetButtonDown("Jump"))
        {
            //Call Jump if player is grounded
            if (IsGrounded())
            {
                Jump(); 
            }
            else
            {
                //Let player jump again if still able to double jump 
                if (canDoubleJump)
                {
                    Jump();

                    //Disable doublejump
                    canDoubleJump = false;
                }
            }
        }

        //Different jump height depending on button press
        if (Input.GetButtonUp("Jump") && rb2d.velocity.y > 0)
        {
            rb2d.velocity = rb2d.velocity * 0.5f;
        }

        //Dragging player to ground when falling depending on fallmultiplier 
        if (rb2d.velocity.y < 0)
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }


        //Health System
        if (health > numberofHearts)
        {
            health = numberofHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            if (i < numberofHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }

        if (health <= 0)
        {
            SceneManager.LoadScene(2, LoadSceneMode.Single);
            Debug.Log("Loading");
        }
    }

    //Move Player
    private void FixedUpdate()
    {
        Walk();
    }

    //Change velocity of rigidbody depending on speed and move input
    private void Walk()
    {
        rb2d.velocity = new Vector2(moveInput * moveSpeed, rb2d.velocity.y);
    }

    //Flip function 
    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }

    //Jump function
    private void Jump()
    {
        //Set velocity of rigidbody
        if (Flipped == true)
        {
            rb2d.velocity = Vector2.down * jumpForce;
        }
        else
        {
            rb2d.velocity = Vector2.up * jumpForce;
        }
    }

    //Groundcheck
    private bool IsGrounded()
    {
        //Shoot raycast from center downwards 
        //check for ground layer 
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.size - new Vector3(0.01f, 0f, 0f), 0, Vector2.down, 0.1f, groundLayerMask);

        return raycastHit.collider != null;
    }

    //Damage player on collision with enemy
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            health--;
            Destroy(collision.gameObject); 
        }

        if (collision.tag == "PowerUp")
        {
            health++;
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Grav")
        {
            rb2d.gravityScale *= -1;
            Flipped = !Flipped;

            Vector3 scaler = transform.localScale;
            scaler.y *= -1;
            transform.localScale = scaler;
        }
    }
}
